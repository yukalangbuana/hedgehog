from django.shortcuts import render
from rest_framework import viewsets
from .models import aapl
from .serializers import aaplSerializer

# Create your views here.
class aaplView(viewsets.ModelViewSet):
    queryset = aapl.objects.all()
    serializer_class = aaplSerializer
