# Generated by Django 2.0.6 on 2018-12-31 07:07

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='aapl',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.CharField(max_length=60)),
                ('open_price', models.CharField(max_length=60)),
                ('high_price', models.CharField(max_length=60)),
                ('low_price', models.CharField(max_length=60)),
                ('close_price', models.CharField(max_length=60)),
                ('volume', models.CharField(max_length=60)),
            ],
        ),
    ]
