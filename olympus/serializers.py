from rest_framework import serializers
from .models import aapl

class aaplSerializer(serializers.ModelSerializer):
    class Meta:
        model = aapl
        fields = '__all__'